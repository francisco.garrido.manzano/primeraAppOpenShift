const express = require('express');
const app = express();

app.get('/', (req,res) =>{
  res.send('recibido 2');
});

app.post('/', (req,res) =>{
  res.send('guardando');
});

app.put('/', (req,res) =>{
  res.send('actualizando');
});

app.delete('/', (req,res) =>{
  res.send('eliminando');
});

app.listen(8080,() => {
console.log('server on port 8080');
});
